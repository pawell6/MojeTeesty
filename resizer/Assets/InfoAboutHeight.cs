﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoAboutHeight : MonoBehaviour {

    private TextMesh textMesh; 
	void Start () {
        textMesh = GetComponent<TextMesh>();
        textMesh.text = "0";
	}
	
	// Update is called once per frame
	void Update () {

        float Cubescale = transform.parent.GetComponent<Transform>().localScale.y;
        textMesh.text = Cubescale.ToString();
	}
}
