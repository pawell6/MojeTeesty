﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.

using HoloToolkit.Sharing;
using HoloToolkit.Sharing.Tests;
using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
//using SpectatorView;
using UnityEngine;
using HoloToolkit.Sharing.Tests;
using HoloToolkit.Sharing;


public class TapToMoveRabbit : Singleton<TapToMoveRabbit> 
    {
        public LayerMask layerMask;
        public LayerMask notTheselayerMasks;
        public Material[] myMaterialsArray;
        public GameObject pointerWhereToPutBunny;
        public GameObject bunnyPrefab;
        public bool isBunnyPlacingMode = false;

        private bool placeIt = false;

    void Start()
    {
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.InstantiateBunny] = ReceivedInfoInstantiateBunny;
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.InstantiateBunnyHighlighter] = ReceivedInfoBunnyInstatiateHiglighter;
    }

    void Update()
        {

            if (isBunnyPlacingMode == true)
            {
                        // Do a raycast into the world that will only hit the Spatial Mapping mesh.
                   Vector3 headPosition = Camera.main.transform.position;
                   Vector3 gazeDirection = Camera.main.transform.forward;
                   pointerWhereToPutBunny.SetActive(true);
                   RaycastHit hitInfo;
                    if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 30.0f, layerMask))
                    {
                        
                        pointerWhereToPutBunny.GetComponent<MeshRenderer>().material = myMaterialsArray[0];
                        pointerWhereToPutBunny.transform.position = hitInfo.point;

                        CustomMessages.Instance.SendBunnyInstantiatePlaceHighlighter(hitInfo.point,0);


                        if (placeIt == true)
                        {
                            // Quaternion toQuat = new Quaternion();
                            //toQuat.x = -90;
                            
                            GameObject bunny = Instantiate(bunnyPrefab, hitInfo.point,Quaternion.identity);
                            float randomMove = Random.Range(0, 360);
                            bunny.transform.Rotate(-90, 0, randomMove);
                            
                             placeIt = false;
                             CustomMessages.Instance.SendBunnyInstantiate(hitInfo.point,randomMove);
                           }
                    }

                    RaycastHit hitInfo2;
                    if (Physics.Raycast(headPosition, gazeDirection, out hitInfo2, 30.0f, notTheselayerMasks))
                    {
                                       
                            pointerWhereToPutBunny.GetComponent<MeshRenderer>().material = myMaterialsArray[1];           
                            pointerWhereToPutBunny.transform.position = hitInfo2.point;

                            CustomMessages.Instance.SendBunnyInstantiatePlaceHighlighter(hitInfo.point, 1);

            }


           
            }

        
        }

        public void ClickedRightPlace()
        {
            placeIt = true;
     
        }

        public void ReceivedInfoBunnyInstatiateHiglighter(NetworkInMessage msg)
         {
                msg.ReadInt64();

                Vector3 positionHighlighter = CustomMessages.Instance.ReadVector3(msg);
                int colorNr = msg.ReadInt32();

                pointerWhereToPutBunny.transform.position = positionHighlighter;
                pointerWhereToPutBunny.GetComponent<MeshRenderer>().material = myMaterialsArray[colorNr];
         }

        public void ReceivedInfoInstantiateBunny(NetworkInMessage msg)
        {
            msg.ReadInt64();

            Vector3 instantiatePosition = CustomMessages.Instance.ReadVector3(msg);
            float random = msg.ReadFloat();        

            GameObject bunny = Instantiate(bunnyPrefab, instantiatePosition, Quaternion.identity);
            bunny.transform.Rotate(-90, 0, random);

          }

}