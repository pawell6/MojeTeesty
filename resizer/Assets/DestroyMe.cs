﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMe : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine("Die");
	}
	
	IEnumerator Die()
    {
        yield return new WaitForSeconds(7f);
        Destroy(this.gameObject);
    }
}
