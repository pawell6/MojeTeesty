﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale : Singleton<Scale> {

    public GameObject deer;
    public GameObject bush;
    public TextMesh[] textMeshTab;
    public Transform placeForTextSpawnAndSplitUp;
    public bool isScaleUpDeer = false;
    public bool isScaleDownBush = false;

    private float speed = 8f;

    //public void ScaleDownDeer()
    //{
    //    deer.transform.localScale -= new Vector3(0.05f,0.05f,0.05f);
    //    deer.transform.position -= new Vector3(0, 0.025f, 0);

    //    if (deer.transform.localScale.x <= 0.5f)
    //    {
    //        scaleDownDeer = false;
    //    }
    //}

    public void PlayDeerSounds()
    {
        deer.GetComponent<AudioSource>().Play();

        foreach (var item in textMeshTab)
        {
           TextMesh textClone = Instantiate(item, placeForTextSpawnAndSplitUp.position,Quaternion.identity);
            textClone.transform.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(0, 3) * speed, Random.Range(0, 3) * speed, Random.Range(0, 3) * speed));
        }
    }

    public void ScaleDownBush()
    {
        bush.transform.localScale -= new Vector3(0.05f, 0.05f, 0.05f);
        bush.transform.position -= new Vector3(0, 0.025f, 0);

        if (bush.transform.localScale.x <= 0.5f)
        {
           isScaleDownBush = false;
        }
    }

    public void ScaleUpDeer()
    {
        deer.transform.localScale += new Vector3(0.05f, 0.05f, 0.05f);
        deer.transform.position += new Vector3(0,0.025f,0);

        if(deer.transform.localScale.x >= 1.8f)
        {
            isScaleUpDeer = false;
        }
    }

    private void FixedUpdate()
    {
        if(isScaleUpDeer == true)
        {
            ScaleUpDeer();
        }

        if (isScaleDownBush == true)
        {
            ScaleDownBush();
        }
    }

}
