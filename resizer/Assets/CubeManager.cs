﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.SpatialMapping;
using HoloToolkit.Unity.InputModule;
using HoloToolkit.Sharing.Tests;
using HoloToolkit.Sharing;
public class CubeManager : MonoBehaviour {

    RaycastHit hitInfo;

    private void Start()
    {
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.StrechCubeUp] = OnStrechUp;
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.ScaleCubeUp] = OnMakeBigger;
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.ScaleCubeDown] = OnMakeSmaller;
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.StrechCubeDown] = OnStrechDown;
    }
    public void OnStrechUp(NetworkInMessage msg)
    {
        msg.ReadInt64();

        var ReceivedScaleVector = CustomMessages.Instance.ReadVector3(msg);
        var ReceivedCubeTag = msg.ReadString();
        Debug.Log("Received Cube Tag : " + ReceivedCubeTag);
        var CubeToBeChanged = GameObject.FindGameObjectWithTag(ReceivedCubeTag);

        CubeToBeChanged.transform.localScale = ReceivedScaleVector;
        CubeToBeChanged.transform.localPosition += new Vector3(0f, 0.1f, 0f);

    }
    public void OnStrechDown(NetworkInMessage msg)
    {
        msg.ReadInt64();

        var ReceivedScaleVector = CustomMessages.Instance.ReadVector3(msg);
        var ReceivedCubeTag = msg.ReadString();
        Debug.Log("Received Cube Tag : " + ReceivedCubeTag);
        var CubeToBeChanged = GameObject.FindGameObjectWithTag(ReceivedCubeTag);

        CubeToBeChanged.transform.localScale = ReceivedScaleVector;
        CubeToBeChanged.transform.localPosition += new Vector3(0f, 0.1f, 0f);

    }

    public void OnMakeBigger(NetworkInMessage msg)
    {
        msg.ReadInt64();

        var ReceivedScaleVector = CustomMessages.Instance.ReadVector3(msg);
        var ReceivedCubeTag = msg.ReadString();
        Debug.Log("Received Cube Tag : " + ReceivedCubeTag);
        var CubeToBeChanged = GameObject.FindGameObjectWithTag(ReceivedCubeTag);

        CubeToBeChanged.transform.localScale = ReceivedScaleVector;
        CubeToBeChanged.transform.localPosition += new Vector3(0f, 0.1f, 0f);
    }
    public void OnMakeSmaller(NetworkInMessage msg)
    {
        msg.ReadInt64();

        var ReceivedScaleVector = CustomMessages.Instance.ReadVector3(msg);
        var ReceivedCubeTag = msg.ReadString();
        Debug.Log("Received Cube Tag : " + ReceivedCubeTag);
        var CubeToBeChanged = GameObject.FindGameObjectWithTag(ReceivedCubeTag);

        CubeToBeChanged.transform.localScale = ReceivedScaleVector;
        CubeToBeChanged.transform.localPosition -= new Vector3(0f, 0.1f, 0f);

    }









    public void StrechUp()
    {
        Vector3 headPosition = Camera.main.transform.position;
        Vector3 gazeDirection = Camera.main.transform.forward;
        if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 30.0f))
        {
            var CubeHit = hitInfo.collider.gameObject;
            var CubeHitTag = CubeHit.tag;

            CubeHit.transform.localScale += new Vector3(0f, 0.2f, 0f);
            CubeHit.transform.localPosition += new Vector3(0f, 0.1f, 0f);

            CustomMessages.Instance.StrechCubeUp(CubeHit.transform.localScale, CubeHitTag);
        }
        else
        {
            Debug.Log("Raycast didn't hit anything");
        }   
    }
    public void ScaleUp()
    {
        Vector3 headPosition = Camera.main.transform.position;
        Vector3 gazeDirection = Camera.main.transform.forward;
        if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 30.0f))
        {
            var CubeHit = hitInfo.collider.gameObject;
            var CubeHitTag = CubeHit.tag;

            CubeHit.transform.localScale += new Vector3(0.2f, 0.2f, 0.2f);
            CubeHit.transform.localPosition += new Vector3(0f, 0.1f, 0f);

            CustomMessages.Instance.MakeBigger(CubeHit.transform.localScale, CubeHitTag);
        }
        else
        {
            Debug.Log("Raycast didn't hit anything");
        }
    }
    public void ScaleDown()
    {
        Vector3 headPosition = Camera.main.transform.position;
        Vector3 gazeDirection = Camera.main.transform.forward;
        if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 30.0f))
        {
            var CubeHit = hitInfo.collider.gameObject;
            var CubeHitTag = CubeHit.tag;

            CubeHit.transform.localScale -= new Vector3(0.2f, 0.2f, 0.2f);
            CubeHit.transform.localPosition -= new Vector3(0f, 0.1f, 0f);

            CustomMessages.Instance.MakeSmaller(CubeHit.transform.localScale, CubeHitTag);
        }
        else
        {
            Debug.Log("Raycast didn't hit anything");
        }
    }
    public void StrechDown()
    {
        Vector3 headPosition = Camera.main.transform.position;
        Vector3 gazeDirection = Camera.main.transform.forward;
        if (Physics.Raycast(headPosition, gazeDirection, out hitInfo, 30.0f))
        {
            var CubeHit = hitInfo.collider.gameObject;
            var CubeHitTag = CubeHit.tag;

            CubeHit.transform.localScale -= new Vector3(0f, 0.2f, 0f);
            CubeHit.transform.localPosition -= new Vector3(0f, 0.1f, 0f);

            CustomMessages.Instance.StrechCubeDown(CubeHit.transform.localScale, CubeHitTag);
        }
        else
        {
            Debug.Log("Raycast didn't hit anything");
        }
    }



}
