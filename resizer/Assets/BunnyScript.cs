﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using System;
using HoloToolkit.Sharing.Tests;
using HoloToolkit.Sharing;

public class BunnyScript : MonoBehaviour, IInputClickHandler, INavigationHandler
{
    public GameObject pointerWhereToPutBunny;
    public Transform positionForRabbitInTheAir;
    public float speed = 0.5f;

    private bool isInTheAir = false;
    private bool isClicked = false;
    private bool isClicked2 = false;
    private float rotationFactor, rotationFactor2;
    private Vector3 startPosition;
    private float rotationSensitivity = 2f;
    private bool isItFirstTime;

    void Start()
    {
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.MoveBunnyInTheAir] = ReceivedInfoTapBunny;
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.RotateBunnyInTheAir] = ReceivedInfoRotateBunny;

        startPosition = transform.position;
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        isClicked = !isClicked;

        if (isItFirstTime)
        {
            isClicked2 = !isClicked2;
        }
       
        CustomMessages.Instance.SendBunnyTap();
    }
  
    void Update()
    {
        if (isClicked)
        {
            GetComponent<Rigidbody>().useGravity = false;

            transform.position = Vector3.Lerp(transform.position, positionForRabbitInTheAir.position,Time.deltaTime * 3);

            if (Vector3.Distance(transform.position,positionForRabbitInTheAir.position) <= 0.05f )
            {
                //isClicked = false;
                isInTheAir = true;
                isItFirstTime = true;
            }
        }
        if (isClicked2)
        {
            GetComponent<Rigidbody>().useGravity = true;

            TapToMoveRabbit.Instance.isBunnyPlacingMode = true;
        }

    }

    public void ReceivedInfoTapBunny(NetworkInMessage msg)
    {
        msg.ReadInt64();

        isClicked = !isClicked;

        if (isItFirstTime)
        {
            isClicked2 = !isClicked2;
        }

    }

    public void ReceivedInfoRotateBunny(NetworkInMessage msg)
    {
        msg.ReadInt64();

        Vector3 _rotatedBunny = CustomMessages.Instance.ReadVector3(msg);

        transform.Rotate(_rotatedBunny);

    }


    //not using ,but must be there
    public void OnNavigationCanceled(NavigationEventData eventData)
    {

    }

    public void OnNavigationCompleted(NavigationEventData eventData)
    {

    }
    
    public void OnNavigationStarted(NavigationEventData eventData)
    {
        
    }
    // end of not using 

    public void OnNavigationUpdated(NavigationEventData eventData)
    {
        if (isInTheAir)
        {
             rotationFactor = eventData.CumulativeDelta.x * rotationSensitivity;        
             transform.Rotate(new Vector3(0, 0, rotationFactor));
            Vector3 rotatedBunny = new Vector3(0, 0,rotationFactor);

            CustomMessages.Instance.SendBunnyRotate(rotatedBunny);
        }
    }
}
