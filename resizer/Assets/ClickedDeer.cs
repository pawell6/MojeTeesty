﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using HoloToolkit.Sharing.Tests;
using HoloToolkit.Sharing;

public class ClickedDeer : MonoBehaviour,IInputClickHandler 
{
    private void Start()
    {       
        CustomMessages.Instance.MessageHandlers[CustomMessages.TestMessageID.ScaleDeerUp] = BiggerDeer;        
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {        
        Scale.Instance.isScaleUpDeer = true;
        Scale.Instance.isScaleDownBush = true;
        Scale.Instance.PlayDeerSounds();

        CustomMessages.Instance.DeerScaleUp();
    }

    public void BiggerDeer(NetworkInMessage msg)
    {
        msg.ReadInt64();

        Scale.Instance.isScaleUpDeer = true;
        Scale.Instance.isScaleDownBush = true;
        Scale.Instance.PlayDeerSounds();
    }
  
}
